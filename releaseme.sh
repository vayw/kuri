#! /bin/bash

set -e
shopt -s expand_aliases

# colors
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
COLOR_OFF='\033[0m'

GIT_TOPLEVEL=$(git rev-parse --show-toplevel)
VERSION="0.0.0"

DO_RELEASE=false
DO_PREPARATORY_WORK=false

OPTION_COUNTER=0
while (( "$#" )); do
    ((OPTION_COUNTER+=1))
    if [[ $OPTION_COUNTER -gt 1 ]]
    then
        echo -e "${RED}Error:${COLOR_OFF} Too many arguments specified! Try '--help' for more information."
        exit -1
    fi

    case $1 in
        -h|--help)
            echo "Kuri Release Script"
            echo ""
            echo "Usage: releaseme.sh [option]"
            echo "Only one option at a time is allowed!"
            echo "Options:"
            echo "    -h, --help                        Prints this help"
            echo "    -p, --preparatory-work <VERSION>  Performs release preparatory work like"
            echo "                                      setting the specified VERSION,"
            echo "                                      building and running on the emulator"
            echo "    -r, --release                     Creates a git commit, git tag and the RPMs"
            echo ""
            echo "Example:"
            echo "    releaseme.sh --release 0.4.1"
            exit 0
            ;;
        -p|--preparatory-work)
            if [[ $# -ne 2 ]]
            then
                echo -e "${RED}Error:${COLOR_OFF} No parameter specified! Try '--help' for more information."
            fi
            VERSION="$2"

            DO_PREPARATORY_WORK=true
            shift 2
            ;;
        -r|--release)
            DO_RELEASE=true
            shift 1
            ;;
        *)
            echo "Invalid argument '$1'. Try '--help' for options."
            exit -1
            ;;
    esac
done

alias sfdk=~/SailfishOS/bin/sfdk

if [[ $DO_PREPARATORY_WORK == true ]]
then
    cd $GIT_TOPLEVEL

    # set version
    sed -i "s/set(KURI_VERSION \(.*\))/set(KURI_VERSION ${VERSION})/g" CMakeLists.txt
    sed -i "s/Version: \(.*\)/Version: ${VERSION}/g" rpm/harbour-kuri.yaml

    while true; do
        read -p "Edit 'rpm/harbour-kuri.changes'? (Y/N) [default=Y]: " yn
        case $yn in
            [Yy]* | "")
                edit rpm/harbour-kuri.changes
                break
                ;;
            [Nn]*)
                break
                ;;
            *) echo "Please use either 'Y' or 'N'.";;
        esac
    done

    while true; do
        echo "Did the minimal supported SFOS version change?"
        read -p "Edit 'rpm/harbour-kuri.yaml' and set 'sailfish-version'? (Y/N) [default=N]: " yn
        case $yn in
            [Yy]*)
                edit rpm/harbour-kuri.yaml
                break
                ;;
            [Nn]* | "")
                break
                ;;
            *) echo "Please use either 'Y' or 'N'.";;
        esac
    done

    # build for minimal supported SFOS version on x64
    cd $GIT_TOPLEVEL
    mkdir -p build/x64
    rm -rf build/x64/*
    cd build/x64

    sfdk config target=SailfishOS-3.0.0.8-i486
    sfdk config device="Sailfish OS Emulator 3.0.0.8"
    sfdk build -j 4 ../..

    sfdk deploy --sdk
    sfdk device exec /usr/bin/harbour-kuri

    exit 0
fi

if [[ $DO_RELEASE == true ]]
then
    cd $GIT_TOPLEVEL

    # try to get version from rpm/harbour-kuri.yaml
    VERSION=$(grep Version rpm/harbour-kuri.yaml | sed "s/Version: //g")

    git add .
    git commit -m"bump version to ${VERSION}"
    git tag "v${VERSION}"

    # build for armv7
    cd $GIT_TOPLEVEL
    mkdir -p build/armv7
    rm -rf build/armv7/*
    cd build/armv7

    sfdk config target=SailfishOS-3.0.0.8-armv7hl
    sfdk build -j 4 ../..

    # build for aarch64
    cd $GIT_TOPLEVEL
    mkdir -p build/aarch64
    rm -rf build/aarch64/*
    cd build/aarch64

    sfdk config target=SailfishOS-4.1.0.24-aarch64
    sfdk build -j 4 ../..

    cd $GIT_TOPLEVEL
    echo -e "${GREEN}Success!${COLOR_OFF} Please push the commit and the git tag to finalize the release and upload the RPMs to openrepos!"

    exit 0
fi
