# Kuri

Kuri is a sport tracking application for Sailfish OS. Kuri means "run" in Esperanto. Funnily it also means "discipline" in Finnish.
Features are:
- recording tracks
- voice coach
- view recorded track on a map and show statistics
- save track as GPX file
- connecting to bluetooth heart rate device
- uploading to Strava and Sport-Tracker.com
- Pebble support (needs Rockpool)
- and more...

This application originates from a fork of Laufhelden by Jens Drescher: https://github.com/jdrescher2006/Laufhelden

# Credits

- Jens Dresher and all the contributors of Laufhelden

Workout icons are from here https://icons8.com (license: https://creativecommons.org/licenses/by-nd/3.0/)

# License

License: GNU General Public License (GNU GPLv3)

# Icon Color Palette

- `#c02222` red
- `#c06622` orange
- `#c0a622` yellow
- `#22c071` green
- `#228bc0` blue
- `#7e22c0` violet
- `#c022c0` purple
- `#707b80` silver
- `#333333` pebble

# Development

## Emulator Setup

In order to run Kuri in the emulator, `mapbox-gl.rpm` must be installed. It can be downloaded from [openrepos](https://openrepos.net/content/rinigus/mapbox-gl-native-bindings-qt-qml). It is recommended to use [v1.5.0](https://openrepos.net/sites/default/files/packages/6933/mapboxgl-qml-1.5.0.1-1.49.1.jolla_.i486.rpm).

Installation procedure:
- get the port which is forwarded to the ssh port on the emulator, e.g. 2223
  - keep in mind that each device emulator might habe it's own distinct port
- copy to emulator
  - run VBox image
  - `scp -P <port> -i <identity file> <mapbox-gl rmp> root@localhost:`
    - latest `scp -P 2223 -i ~/SailfishOS/vmshare/ssh/private_keys/Sailfish_OS-Emulator-latest/root mapboxgl-qml-1.5.0.3-1.51.1.jolla_.i486.rpm root@localhost:`
    - 3.0.0.8 `scp -P 2224 -i ~/SailfishOS/vmshare/ssh/private_keys/Sailfish_OS-Emulator-3.0.0.8/root mapboxgl-qml-1.5.0.3-1.51.1.jolla_.i486.rpm root@localhost:`
    - keep in mind to set the corrept port and identity file
- log in to emulator
  - `ssh -p <port> -i <identity file> root@localhost`
    - latest `ssh -p 2223 -i ~/SailfishOS/vmshare/ssh/private_keys/Sailfish_OS-Emulator-latest/root root@localhost`
    - 3.0.0.8 `ssh -p 2224 -i ~/SailfishOS/vmshare/ssh/private_keys/Sailfish_OS-Emulator-3.0.0.8/root root@localhost`
- install rpm
  - `rpm -i mapboxgl-qml-1.5.0.3-1.51.1.jolla_.i486.rpm`

## Build and Test

`sfdk` is used to build Kuri, see also [sfdk](https://sailfishos.org/wiki/Tutorial_-_Building_packages_-_advanced_techniques)

- create alias for `sfdk`
  - `alias sfdk=~/SailfishOS/bin/sfdk`
- list targets
  - `sfdk tools list`
- set target from within build directory
  - `sfdk config target=SailfishOS-4.1.0.24-i486`
- build
  - `sfdk build -j 4 ..`
  - rpm is in `RPM` folder
- check for compatibility with store
  - `sfdk check RPMS/harbour-kuri.rpm`
- list devices
  - `sfdk device list`
- set emulator
  - `sfdk config device="Sailfish OS Emulator 4.1.0.24"`
- deploy to emulator
  - `sfdk deploy --sdk`
- run in emulator
  - `sfdk device exec /usr/bin/harbour-kuri`
