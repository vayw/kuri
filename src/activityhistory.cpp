/*
 * Copyright (C) 2017-2018 Jens Drescher, Germany
 * Copyright (C) 2021 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "activityhistory.h"

#include "formatter.h"
#include "trackloader.h"

#include <QDebug>
#include <QDir>
#include <QHash>
#include <QStandardPaths>

TrackItem loadTrack(QString trackFilePath) {
    TrackItem   track;
    TrackLoader loader;
    loader.setFilename(trackFilePath);
    track.filename    = trackFilePath;
    track.name        = loader.name();
    track.workout     = loader.workout();
    track.time        = loader.time();
    track.duration    = loader.duration();
    track.distance    = loader.distance();
    track.speed       = loader.speed();
    track.description = loader.description();
    track.stKey       = loader.sTworkoutKey(); // Sports-Tracker workoutkey

    // Get file properties
    QString   dirName      = QStandardPaths::writableLocation(QStandardPaths::HomeLocation) + "/Laufhelden";
    QString   fullFilename = dirName + "/" + trackFilePath;
    QFileInfo infoObject(fullFilename);
    track.fileSize         = QString::number(infoObject.size());
    track.fileLastModified = infoObject.lastModified().toString(Qt::ISODate);

    return track;
}

ActivityHistory::ActivityHistory(QObject* parent)
    : QSortFilterProxyModel(parent) {
    // Maybe that helps reading filenames with umlauts?
    // QTextCodec::codecForName("UTF-8");

    setSourceModel(&m_model);
    setFilterRole(Model::HistoryRoles::WorkoutRole);
    setSortRole(Model::HistoryRoles::DateTimeRole);
    constexpr int COLUMN {0};
    sort(COLUMN, Qt::DescendingOrder);

    connect(&m_model, &Model::dataChanged, this, &ActivityHistory::handleModelDataChange);

    connect(&m_trackLoading, &QFutureWatcher<TrackItem>::resultReadyAt, this, &ActivityHistory::newTrackData);
    connect(&m_trackLoading, &QFutureWatcher<TrackItem>::finished, this, &ActivityHistory::loadingFinished);

    loadAccelerationFile();
    loadAllFiles();
}

ActivityHistory::~ActivityHistory() {
    m_trackLoading.cancel();
    m_trackLoading.waitForFinished();
    saveAccelerationFile();
}

ActivityHistory::Model::Model(QObject* parent)
    : QAbstractListModel(parent) {}

QHash<int, QByteArray> ActivityHistory::Model::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[Qt::DisplayRole] = "name";
    roles[FilenameRole]    = "filename";
    roles[WorkoutRole]     = "workout";
    roles[DateTimeRole]    = "dateTime";
    roles[DurationRole]    = "duration";
    roles[DistanceRole]    = "distance";
    roles[SpeedRole]       = "speed";
    roles[DescriptionRole] = "description";

    return roles;
}

int ActivityHistory::Model::rowCount(const QModelIndex&) const {
    return m_trackList.count();
}

QVariant ActivityHistory::Model::data(const QModelIndex& index, int role) const {
    auto row = index.row();
    if (row < 0 || row >= m_trackList.size()) { return QVariant(); }

    switch (role) {
        case Qt::DisplayRole:
            return m_trackList[row].name;
        case FilenameRole:
            return m_trackList[row].filename;
        case WorkoutRole:
            return m_trackList[row].workout;
        case DateTimeRole:
            return m_trackList[row].time.toString("yyyy-MM-dd HH:mm");
        case DurationRole: {
            uint hours   = m_trackList[row].duration / (60 * 60);
            uint minutes = (m_trackList[row].duration - hours * 60 * 60) / 60;
            uint seconds = m_trackList[row].duration - hours * 60 * 60 - minutes * 60;
            return QString("%1:%2:%3").arg(hours, 2, 10, QLatin1Char('0')).arg(minutes, 2, 10, QLatin1Char('0')).arg(seconds, 2, 10, QLatin1Char('0'));
        }
        case DistanceRole:
            return m_trackList[row].distance;
        case SpeedRole:
            return (m_trackList[row].speed * 3.6);
        case DescriptionRole:
            return m_trackList[row].description;
        default:
            return QVariant();
    }
    return QVariant();
}

QVariant ActivityHistory::Model::headerData(int section, Qt::Orientation orientation, int role) const {
    if (role != Qt::DisplayRole) { return QVariant(); }
    if (orientation == Qt::Horizontal) {
        return QString("Column %1").arg(section);
    } else {
        return QString("Row %1").arg(section);
    }
}

bool ActivityHistory::Model::setData(const QModelIndex& index, const QVariant& value, int role) {
    auto row = index.row();
    if (row < 0 || row >= m_trackList.size()) { return false; }

    switch (role) {
        case Qt::DisplayRole:
            m_trackList[row].name = value.toString();
            break;
        case FilenameRole:
            return false;
        case WorkoutRole:
            m_trackList[row].workout = value.toString();
            break;
        case DateTimeRole:
            return false;
        case DurationRole:
            return false;
        case DistanceRole:
            return false;
        case SpeedRole:
            return false;
        case DescriptionRole:
            m_trackList[row].description = value.toString();
            break;
        default:
            return false;
    }

    emit dataChanged(index, index, {role});
    return true;
}

Qt::ItemFlags ActivityHistory::Model::flags(const QModelIndex& index) const {
    auto row = index.row();
    if (row < 0 || row >= m_trackList.count()) { return Qt::NoItemFlags; }

    return Qt::ItemIsEditable | QAbstractListModel::flags(index);
}

const QList<TrackItem>& ActivityHistory::Model::tracks() const {
    return m_trackList;
}

void ActivityHistory::Model::addTrack(const TrackItem track) {
    auto rowToInsert = m_trackList.count();
    beginInsertRows(QModelIndex(), rowToInsert, rowToInsert);
    m_trackList.push_back(track);
    endInsertRows();
}

void ActivityHistory::Model::removeTrack(int row) {
    beginRemoveRows(QModelIndex(), row, row);
    m_trackList.removeAt(row);
    endRemoveRows();
}

qreal ActivityHistory::distance() {
    return m_workoutDistance;
}

QString ActivityHistory::getSportsTrackerKey(const int sourceRow) const {
    if (sourceRow < 0 || sourceRow >= m_model.tracks().count()) { return ""; }
    return m_model.tracks().at(sourceRow).stKey;
}

QString ActivityHistory::duration() const {
    return Formatter::secondsToHoursMinutesSeconds(m_workoutDuration);
}

int ActivityHistory::mapToSourceRow(int row) {
    constexpr int COLUMN {0};
    auto          modelIndex = this->index(row, COLUMN);
    return mapToSource(modelIndex).row();
}

QString ActivityHistory::workouttypeAt(int row) {
    return m_model.tracks().at(mapToSourceRow(row)).workout;
}
int ActivityHistory::durationAt(int row) {
    return m_model.tracks().at(mapToSourceRow(row)).duration;
}
qreal ActivityHistory::distanceAt(int row) {
    return m_model.tracks().at(mapToSourceRow(row)).distance;
}
QDateTime ActivityHistory::dateAt(int row) {
    return m_model.tracks().at(mapToSourceRow(row)).time;
}

int ActivityHistory::numberOfTracksToLoad() const {
    return m_numberOfTracksToLoad;
}
int ActivityHistory::numberOfLoadedTracks() const {
    return m_model.tracks().count();
}

bool ActivityHistory::varyingWorkoutTypes() const {
    return m_varyingWorkoutTypes;
}

void ActivityHistory::updateVaryingWorkoutTypes() {
    bool    first = true;
    QString firstTrackWorkout;
    for (const auto& track : m_model.tracks()) {
        if (first) {
            first             = false;
            firstTrackWorkout = track.workout;
        } else {
            if (track.workout != firstTrackWorkout) {
                if (!m_varyingWorkoutTypes) {
                    m_varyingWorkoutTypes = true;
                    emit varyingWorkoutTypesChanged();
                }
                return;
            }
        }
    }
    if (m_varyingWorkoutTypes) {
        m_varyingWorkoutTypes = false;
        emit varyingWorkoutTypesChanged();
    }
}

bool ActivityHistory::removeTrack(int row) {
    QString dirName = QStandardPaths::writableLocation(QStandardPaths::HomeLocation) + "/Laufhelden";
    QDir    dir     = QDir(dirName);
    if (!dir.exists()) {
        qDebug() << "Directory doesn't exist";
        return false;
    }

    auto    sourceRow = mapToSourceRow(row);
    QString filename  = m_model.tracks().at(sourceRow).filename;
    bool    success   = dir.remove(filename);
    if (success) {
        m_model.removeTrack(sourceRow);
        qDebug() << "Removed:" << filename;

        // propagate the changes in the number of tracks
        m_numberOfTracksToLoad = m_model.tracks().count();
        emit numberOfTracksToLoadChanged();
        emit numberOfLoadedTracksChanged();

        m_gpxFilesChanged = true;
        saveAccelerationFile();

        updateVaryingWorkoutTypes();

        return true;
    } else {
        qDebug() << "Removing failed:" << filename;
        return false;
    }
}

void ActivityHistory::newTrackData(int num) {
    TrackItem track(m_trackLoading.resultAt(num));
    qDebug() << "Finished loading of " << track.filename;

    m_model.addTrack(track);

    emit numberOfLoadedTracksChanged();
}

void ActivityHistory::loadingFinished() {
    qDebug() << "Data loading finished";

    m_workoutDuration = 0;
    m_workoutDistance = 0.0;

    // Get workout time

    for (const auto& track : m_model.tracks()) {
        m_workoutDuration = m_workoutDuration + track.duration;
        m_workoutDistance = m_workoutDistance + track.distance;
    }

    saveAccelerationFile();
    updateVaryingWorkoutTypes();

    emit trackLoadingFinished();
}

void ActivityHistory::handleModelDataChange(const QModelIndex&, const QModelIndex&, const QVector<int>& roles) {
    // setting model data means they are also synced to the gpx file
    m_gpxFilesChanged = true;

    for (const auto role : roles) {
        if (role == Model::HistoryRoles::WorkoutRole) {
            updateVaryingWorkoutTypes();
            break;
        }
    }
}

void ActivityHistory::loadAccelerationFile() {
    QString dirName      = QStandardPaths::writableLocation(QStandardPaths::HomeLocation) + "/Laufhelden";
    QString fullFilename = dirName + "/AccelerationHelper.dat";
    qDebug() << "Reading helper file:" << fullFilename;

    QFile fileObject(fullFilename);

    if (!fileObject.exists()) {
        qDebug() << "No acceleration file found!";
        return;
    }

    if (!fileObject.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "Acceleration file opening failed, aborting";
        return;
    }

    QTextStream streamObject(&fileObject);
    streamObject.setCodec("UTF-8");
    TrackItem track;

    while (!streamObject.atEnd()) {
        QString sLine = streamObject.readLine();

        // Check if we are at the start of a new track item.
        if (sLine.startsWith("1: ")) {
            track.filename = sLine.mid(3);
            track.time     = QDateTime();
        } else if (sLine.startsWith("2: ")) {
            track.name = sLine.mid(3);
        } else if (sLine.startsWith("3: ")) {
            track.workout = sLine.mid(3);
        } else if (sLine.startsWith("4: ")) {
            track.time = QDateTime::fromString(sLine.mid(3), Qt::ISODate);
        } else if (sLine.startsWith("5: ")) {
            track.duration = sLine.mid(3).toInt();
        } else if (sLine.startsWith("6: ")) {
            track.distance = sLine.mid(3).toDouble();
        } else if (sLine.startsWith("7: ")) {
            track.speed = sLine.mid(3).toDouble();
        } else if (sLine.startsWith("8: ")) {
            track.description = sLine.mid(3);
        } else if (sLine.startsWith("9: ")) {
            track.fileSize = sLine.mid(3);
        } else if (sLine.startsWith("10: ")) {
            track.fileLastModified = sLine.mid(4);
        } else if (sLine.startsWith("11: ")) {
            // Now, this is the last parameter for this track item.
            track.stKey = sLine.mid(4);


            // Get file properties
            QString dirName = QStandardPaths::writableLocation(QStandardPaths::HomeLocation) + "/Laufhelden";
            fullFilename    = dirName + "/" + track.filename;
            QFileInfo infoObject(fullFilename);

            // Check if file exists
            if (infoObject.exists()) {
                // OK, file exists. Now check if file is consistent
                QString sFileSize     = QString::number(infoObject.size());
                QString sLastModified = infoObject.lastModified().toString(Qt::ISODate);

                if (sFileSize == track.fileSize && sLastModified == track.fileLastModified) {
                    m_model.addTrack(track);
                } else {
                    // tell saveAccelerationFile about changes
                    m_gpxFilesChanged = true;
                    qDebug() << "File not consistent: " << track.filename;
                    qDebug() << "sFileSize/item.fileSize: " << sFileSize << "/" << track.fileSize;
                    qDebug() << "sLastModified/item.fileLastModified: " << sLastModified << "/" << track.fileLastModified;
                }
            } else {
                // tell saveAccelerationFile about changes
                m_gpxFilesChanged = true;
                qDebug() << "File not existant: " << track.filename;
            }
        }
    }

    // we already loaded some tracks, therefore propagate the changes
    m_numberOfTracksToLoad = m_model.tracks().count();
    emit numberOfTracksToLoadChanged();
    emit numberOfLoadedTracksChanged();
}

void ActivityHistory::saveAccelerationFile() {
    // If there are no changes in the GPX files, don't do anything here
    if (m_gpxFilesChanged == false) { return; }

    if (m_model.tracks().count() <= 0) { return; }

    QString dirName      = QStandardPaths::writableLocation(QStandardPaths::HomeLocation) + "/Laufhelden";
    QString fullFilename = dirName + "/AccelerationHelper.dat";
    qDebug() << "Writing helper file:" << fullFilename;

    QFile fileObject(fullFilename);

    if (fileObject.open(QFile::WriteOnly | QFile::Text)) {
        QTextStream streamObject(&fileObject);
        streamObject.setCodec("UTF-8");

        for (const auto& track : m_model.tracks()) {
            streamObject << "1: " << track.filename << '\n';
            streamObject << "2: " << track.name << '\n';
            streamObject << "3: " << track.workout << '\n';
            streamObject << "4: " << track.time.toString(Qt::ISODate) << '\n';
            streamObject << "5: " << track.duration << '\n';
            streamObject << "6: " << track.distance << '\n';
            streamObject << "7: " << track.speed << '\n';
            streamObject << "8: " << track.description << '\n';
            streamObject << "9: " << track.fileSize << '\n';
            streamObject << "10: " << track.fileLastModified << '\n';
            streamObject << "11: " << track.stKey << '\n';
            streamObject << '\n';
        }
    } else {
        qDebug() << "error opening helper file\n";
        return;
    }

    fileObject.flush();
    fileObject.close();
}

void ActivityHistory::loadFile(QString fileName) {
    m_trackFilesToLoad.push_back(fileName);

    loadTracks();
}

void ActivityHistory::loadAllFiles() {
    QString dirName = QStandardPaths::writableLocation(QStandardPaths::HomeLocation) + "/Laufhelden";
    QDir    dir     = QDir(dirName);
    if (!dir.exists()) {
        qDebug() << "Directory doesn't exist, nothing to load";
        return;
    }
    dir.setFilter(QDir::Files);
    dir.setSorting(QDir::Name | QDir::Reversed);
    dir.setNameFilters(QStringList("*.gpx"));
    m_trackFilesToLoad = dir.entryList();

    loadTracks();
}

void ActivityHistory::loadTracks() {
    if (m_trackLoading.isRunning()) {
        m_trackLoading.cancel();
        m_trackLoading.waitForFinished();
    }

    QVector<int> alreadyLoaded;
    int          i = 0;
    for (const auto& trackFileTLoad : m_trackFilesToLoad) {
        for (const auto& track : m_model.tracks()) {
            if (track.filename == trackFileTLoad) {
                alreadyLoaded.push_front(i);
                break;
            }
        }
        ++i;
    }

    for (auto i : alreadyLoaded) {
        m_trackFilesToLoad.removeAt(i);
    }

    // tell saveAccelerationFile about changes
    m_gpxFilesChanged = !m_trackFilesToLoad.empty();

    int numberOfTracksToLoad = m_trackFilesToLoad.count() + m_model.tracks().count();
    if (numberOfTracksToLoad != m_numberOfTracksToLoad) {
        m_numberOfTracksToLoad = numberOfTracksToLoad;
        emit numberOfTracksToLoadChanged();
    }

    m_trackLoading.setFuture(QtConcurrent::mapped(m_trackFilesToLoad, loadTrack));
}
