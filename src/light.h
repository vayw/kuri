#pragma once

#include <QLightSensor>
#include <QObject>


class Light : public QObject {
    Q_OBJECT

    Q_PROPERTY(qreal brightness READ brightness NOTIFY brightnessChanged)

public:
    explicit Light(QObject* parent = nullptr);
    ~Light() = default;

    qreal brightness(void) const;

public slots:
    void refresh(void);
    void deactivate(void);

signals:
    void brightnessChanged(void);

private:
    qreal    m_brightness {0.};
    QSensor* m_sensor {nullptr};
};
