#pragma once

#include <QGuiApplication>
#include <QObject>
#include <QtQml/QtQml>
#include <QtQuick/QQuickView>

class LogWriter : public QObject {
    Q_OBJECT
public:
    explicit LogWriter(QObject* parent = nullptr);
    Q_INVOKABLE void vWriteData(const QString& msg);
    Q_INVOKABLE void vWriteStart(const QString& msg);
};
