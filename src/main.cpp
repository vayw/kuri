/*
 * Copyright (C) 2017 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "activityhistory.h"
#include "device.h"
#include "light.h"
#include "logwriter.h"
#include "pebblemanagercomm.h"
#include "pebblewatchcomm.h"
#include "plotwidget.h"
#include "settings.h"
#include "trackloader.h"
#include "trackrecorder.h"

#include "3rdParty/o2/src/o2.h"

#include <sailfishapp.h>

#include <QGuiApplication>
#include <QtGlobal>
#include <QtQml/QtQml>
#include <QtQuick/QQuickView>

#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

QByteArray encryptDecrypt(QByteArray toEncrypt) {
    char       key    = 'K';
    QByteArray output = toEncrypt;

    for (int i = 0; i < toEncrypt.size(); i++) {
        output[i] = toEncrypt.at(i) ^ key;
    }

    return output;
}

int main(int argc, char* argv[]) {
    // disable debug logging of category `default` (only qDebug from C++; not console.log) since QGridLayout spams the console
    QLoggingCategory::setFilterRules("default.debug=false\n");

    QGuiApplication* app = SailfishApp::application(argc, argv);

    app->setApplicationName("Kuri");
    app->setApplicationVersion(QString(APP_VERSION));

    qDebug() << app->applicationName() << " version " << app->applicationVersion();

    qmlRegisterType<TrackRecorder>("harbour.kuri", 1, 0, "TrackRecorder");
    qmlRegisterType<TrackLoader>("harbour.kuri", 1, 0, "TrackLoader");
    qmlRegisterType<Settings>("harbour.kuri", 1, 0, "Settings");
    qmlRegisterType<Device, 1>("harbour.kuri", 1, 0, "Device");
    qmlRegisterType<LogWriter, 1>("harbour.kuri", 1, 0, "LogWriter");
    qmlRegisterType<PlotWidget, 1>("harbour.kuri", 1, 0, "PlotWidget");
    qmlRegisterType<Light, 1>("harbour.kuri", 1, 0, "Light");
    qmlRegisterType<PebbleManagerComm, 1>("harbour.kuri", 1, 0, "PebbleManagerComm");
    qmlRegisterType<PebbleWatchComm, 1>("harbour.kuri", 1, 0, "PebbleWatchComm");
    qmlRegisterType<O2>("com.pipacs.o2", 1, 0, "O2");

    qmlRegisterSingletonType(SailfishApp::pathTo("qml/components/RecordPageColorTheme.qml"), "harbour.kuri", 1, 0, "RecordPageColorTheme");

    ActivityHistory history;

    QQuickView* view = SailfishApp::createView();
    view->rootContext()->setContextProperty("appVersion", app->applicationVersion());
    view->rootContext()->setContextProperty("STRAVA_CLIENT_SECRET", encryptDecrypt("}{s{--z*.x{y{ss///x/x){*xz{(|yy/{syr-/})"));
    view->rootContext()->setContextProperty("STRAVA_CLIENT_ID", "13707");
    view->rootContext()->setContextProperty("ActivityHistory", &history);

    view->setSource(SailfishApp::pathTo("qml/harbour-kuri.qml"));
    view->showFullScreen();

    return app->exec();
}
