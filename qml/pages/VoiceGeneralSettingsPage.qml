/*
 * Copyright (C) 2017-2018 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../tools/JSTools.js" as JSTools

Page
{
    id: page
    allowedOrientations: Orientation.Portrait

    SilicaFlickable
    {
        anchors.fill: parent
        contentHeight: column.height + Theme.paddingLarge;
        VerticalScrollDecorator {}
        Column
        {
            id: column
            width: page.width
            spacing: Theme.paddingLarge
            PageHeader
            {
                title: qsTr("Voice coach general settings")
            }

            TextSwitch
            {
                text: qsTr("Pause/resume music player")
                description: qsTr("If enabled, the music player is paused when a voice message is played and afterwards resumed.")
                Component.onCompleted: {
                    checked = settings.voicePauseMusic;
                    checkedChanged.connect(function() {
                        settings.voicePauseMusic = checked;
                    });
                }
            }
            Separator
            {
                color: Theme.highlightColor
                width: parent.width
                horizontalAlignment: Qt.AlignHCenter
            }
            ComboBox
            {
                label: qsTr("Voice language")
                menu: ContextMenu
                {
                    MenuItem { text: qsTr("English male") }
                    MenuItem { text: qsTr("German male") }
                    MenuItem { text: qsTr("Russian male") }
                }
                Component.onCompleted: {
                    currentIndex = settings.voiceLanguage;
                    currentIndexChanged.connect(function() {
                        settings.voiceLanguage = currentIndex;
                    });
                }
            }
            Button
            {
                width: parent.width - Theme.paddingLarge
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Test")
                onClicked:
                {
                    var sVoiceLanguage = "_en_male.wav";
                    //check voice language and generate last part of audio filename
                    if (settings.voiceLanguage === 0)        //english male
                        sVoiceLanguage = "_en_male.wav";
                    else if (settings.voiceLanguage === 1)   //german male
                        sVoiceLanguage = "_de_male.wav";
                    else if (settings.voiceLanguage === 2)   //russian male
                        sVoiceLanguage = "_ru_male.wav";

                    fncPlaySound("audio/hr_toohigh" + sVoiceLanguage);
                }
            }
        }
    }
}

