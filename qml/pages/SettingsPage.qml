/*
 * Copyright (C) 2017 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page
    allowedOrientations: Orientation.Portrait

    SilicaFlickable
    {
        anchors.fill: parent
        contentHeight: column.height + Theme.paddingLarge;
        VerticalScrollDecorator {}
        Column
        {
            id: column
            width: page.width
            spacing: Theme.paddingLarge
            PageHeader
            {
                title: qsTr("General settings")
            }
            ComboBox
            {
                label: qsTr("Unit of measurement")
                description: qsTr("Note that this setting will be applied after restart of the application.")
                menu: ContextMenu
                {
                    MenuItem { text: qsTr("Metric") }
                    MenuItem { text: qsTr("Imperial") }
                }
                Component.onCompleted: {
                    currentIndex = settings.measureSystem;
                    currentIndexChanged.connect(function() {
                        settings.measureSystem = currentIndex;
                    });
                }
            }
            Separator
            {
                color: Theme.highlightColor
                width: parent.width
                horizontalAlignment: Qt.AlignHCenter
            }
            TextSwitch
            {
                text: qsTr("Record page portrait mode")
                description: qsTr("Keep record page in portrait mode.")
                Component.onCompleted: {
                    checked = settings.recordPagePortrait
                    checkedChanged.connect(function() {
                        settings.recordPagePortrait = checked;
                    });
                }
            }
            Separator
            {
                color: Theme.highlightColor
                width: parent.width
                horizontalAlignment: Qt.AlignHCenter
            }
            TextSwitch
            {
                text: qsTr("Automatic night mode")
                description: qsTr("Switch display to night mode if ambiance light is low.")
                Component.onCompleted: {
                    checked = settings.autoNightMode;
                    checkedChanged.connect(function() {
                        settings.autoNightMode = checked;
                    });
                }
            }
            Separator
            {
                color: Theme.highlightColor
                width: parent.width
                horizontalAlignment: Qt.AlignHCenter
            }
            TextSwitch
            {
                text: qsTr("Enable autosave")
                description: qsTr("No need to enter workout name on end of workout.")
                Component.onCompleted: {
                    checked = settings.enableAutosave;
                    checkedChanged.connect(function() {
                        settings.enableAutosave = checked;
                    });
                }
            }
            Separator
            {
                color: Theme.highlightColor
                width: parent.width
                horizontalAlignment: Qt.AlignHCenter
                visible: false
            }
            TextSwitch
            {
                text: qsTr("Write log file")
                description: qsTr("File: $HOME/Laufhelden/log.txt")
                visible: false
                Component.onCompleted: {
                    checked = settings.enableLogFile;
                    checkedChanged.connect(function() {
                        settings.enableLogFile = checked;
                    });
                }
            }
        }
    }
}
