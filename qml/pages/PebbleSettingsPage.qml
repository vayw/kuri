/*
 * Copyright (C) 2017 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../tools/JSTools.js" as JSTools

Page
{
    id: page
    allowedOrientations: Orientation.Portrait

    property int iCheckPebbleStep: 0
    property var sPebbleListArray
    property variant arComboboxStringArray : []

    property var indexValues: [10, 8, 3]

    Component.onCompleted:
    {
        console.log("First Active PebbleSettingsPage");

        //Check if a Pebble watch was found in Rockpool manager
        var sPebbleList = id_PebbleManagerComm.getListWatches();

        var arComboarray = [];

        for (var i = 0; i < sPebbleList.length; i++)
        {
            arComboarray.push(sPebbleList[i]);
        }
        arComboboxStringArray = arComboarray;

        if (sPebbleList !== undefined && sPebbleList.length > 0)
        {
            //There might be more than one pebble found
            if (sPebbleList.length > 1)
            {
                id_CMB_ChoosePebble.visible = true;

                //Now read the last used pebble string from settings
                var sLastUsedPebbleString = settings.pebbleIDstring;

                //Check if the last used pebble string is in the pebble list
                for (var j = 0; j < sPebbleList.length; j++)
                {
                    if (sLastUsedPebbleString === sPebbleList[j])
                    {
                        sPebblePath = sPebbleList[j];
                        //Select item in combobox
                        id_CMB_ChoosePebble.currentIndex = j;
                        break;
                    }
                }
                id_CMB_ChoosePebble.checkedChanged.connect(function() {
                    settings.pebbleIDstring = arComboboxStringArray[id_CMB_ChoosePebble.currentIndex];
                    sPebblePath = arComboboxStringArray[id_CMB_ChoosePebble.currentIndex];
                    id_PebbleWatchComm.setServicePath(sPebblePath);
                });
            }
            else
            {
                //A pebble was found, we have now a DBus path to it.
                //If there are more than one pebble, use the first one.
                sPebblePath = sPebbleList[0];
            }

            //Read version of Rockpool and check if it is sufficient
            fncCheckVersion(id_PebbleManagerComm.getRockpoolVersion());

            //This sets the path with the BT address to the C++ class and inits the DBUS communication object
            if (sPebblePath !== "") id_PebbleWatchComm.setServicePath(sPebblePath);
        }
        else
        {
            //Show error
            id_REC_PebbleAddress.visible = true;

            //Disable pebble support
            settings.enablePebble = false;

            //Disable this dialog
            id_TextSwitch_enablePebble.enabled = false;
        }


        //Set settings to dialog
        id_TextSwitch_enablePebble.checked = settings.enablePebble;
        id_TextSwitch_enablePebble.checkedChanged.connect(function() {
            settings.enablePebble = id_TextSwitch_enablePebble.checked;
        });

        var arraySize = JSTools.arrayPebbleValueTypes.length;

        var arValueTypes = settings.valuePebbleFields.split(",");
        if (arValueTypes !== undefined && arValueTypes !== "" && arValueTypes.length === indexValues.length)
        {
            var index0 = parseInt(arValueTypes[0]);
            index0 = (index0 >= 0 && index0 < arraySize) ? index0 : indexValues[0];

            var index1 = parseInt(arValueTypes[1]);
            index1 = (index1 >= 0 && index1 < arraySize) ? index1 : indexValues[1];

            var index2 = parseInt(arValueTypes[2]);
            index2 = (index2 >= 0 && index2 < arraySize) ? index2 : indexValues[2];

            if (uniqueNonZeroValues(index0, index1, index2))
            {
                indexValues[0] = index0;
                indexValues[1] = index1;
                indexValues[2] = index2;
            }
        }

        id_CMB_ValueField1.currentIndex = indexValues[0];
        id_CMB_ValueField2.currentIndex = indexValues[1];
        id_CMB_ValueField3.currentIndex = indexValues[2];

        id_CMB_ValueField1.currentIndexChanged.connect(handleValueChange);
        id_CMB_ValueField2.currentIndexChanged.connect(handleValueChange);
        id_CMB_ValueField3.currentIndexChanged.connect(handleValueChange);

        JSTools.fncGenerateHelperArray();
    }

    function uniqueNonZeroValues(index0, index1, index2) {
        if(index0 !== 0 && index0 === index1) { return false; }
        if(index1 !== 0 && index1 === index2) { return false; }
        if(index2 !== 0 && index2 === index0) { return false; }
        return true;
    }

    function handleValueChange() {
        var index0 = id_CMB_ValueField1.currentIndex;
        var index1 = id_CMB_ValueField2.currentIndex;
        var index2 = id_CMB_ValueField3.currentIndex;

        // sanity check for unique values
        if (!uniqueNonZeroValues(index0, index1, index2))
        {
            fncShowMessage("Error",qsTr("This value is already assigned!"), 3000);

            id_CMB_ValueField1.currentIndex = indexValues[0];
            id_CMB_ValueField2.currentIndex = indexValues[1];
            id_CMB_ValueField3.currentIndex = indexValues[2];
            return
        }

        settings.valuePebbleFields = index0.toString() + "," + index1.toString() + "," + index2.toString();

        indexValues[0] = index0;
        indexValues[1] = index1;
        indexValues[2] = index2;

        JSTools.fncGenerateHelperArray();
    }

    onStatusChanged:
    {
        //This is loaded everytime the page is displayed
        if (status === PageStatus.Active)
        {
            console.log("Active PebbleSettingsPage");

            //Check if pebble is connected
            if (settings.enablePebble && !bPebbleConnected)
                bPebbleConnected = id_PebbleWatchComm.isConnected();

            //If pebble is connected read name and address
            if (bPebbleConnected)
                sPebbleNameAddress = id_PebbleWatchComm.getName() + ", " + id_PebbleWatchComm.getAddress();
        }
    }

    function fncCheckVersion(sVersion)
    {
        //Check if version is valid
        if (sVersion === undefined || sVersion === "" || sVersion.indexOf(".") === -1 || sVersion.indexOf("-") === -1)
        {
            id_LBL_Rockpool.text = id_LBL_Rockpool.text + "-";
            id_REC_Rockpool.visible = true;

            //Disable pebble support
            settings.enablePebble = false;

            //Disble this dialog
            id_TextSwitch_enablePebble.checked = false;
        }
        else
        {
            //Swap the - for an . so that the release number becomes part of the version number
            var sModVersion = sVersion.replace("-", ".");

            console.log("Modified version: " + sModVersion);

            console.log("Compare: " + fncCompareVersions(sModVersion, "1.3.3"));

            //Check if Rockpool verion is too old
            if (fncCompareVersions(sModVersion, "1.3.3") > 0)
            {
                id_REC_Rockpool.visible = false;
                id_TextSwitch_enablePebble.enabled = true;
            }
            else
            {
                id_LBL_Rockpool.text = id_LBL_Rockpool.text + sVersion;
                id_REC_Rockpool.visible = true;

                settings.enablePebble = false;
                id_TextSwitch_enablePebble.checked = false;
            }
        }
    }

    function fncCompareVersions(a, b)
    {
        var i, diff;
        var regExStrip0 = /(\.0+)+$/;
        var segmentsA = a.replace(regExStrip0, '').split('.');
        var segmentsB = b.replace(regExStrip0, '').split('.');
        var l = Math.min(segmentsA.length, segmentsB.length);

        for (i = 0; i < l; i++) {
            diff = parseInt(segmentsA[i], 10) - parseInt(segmentsB[i], 10);
            if (diff) {
                return diff;
            }
        }
        return segmentsA.length - segmentsB.length;
    }

    Timer
    {
        id: timCheckPebbleTimer
        interval: 1600
        running: (iCheckPebbleStep > 0)
        repeat: true
        onTriggered:
        {
            iCheckPebbleStep++;

            if (iCheckPebbleStep === 2)
            {
                progressBarCheckPebble.label = qsTr("set metric units");
                pebbleComm.fncSendDataToPebbleApp("4dab81a6-d2fc-458a-992c-7a1f3b96a970", {'3': 1});
            }
            if (iCheckPebbleStep === 3)
            {
                progressBarCheckPebble.label = qsTr("sending data 1");
                pebbleComm.fncSendDataToPebbleApp("4dab81a6-d2fc-458a-992c-7a1f3b96a970", {'0': '02:30', '1': '10.12', '2': '2.4'});
            }
            if (iCheckPebbleStep === 4)
            {
                progressBarCheckPebble.label = qsTr("sending data 2");
                pebbleComm.fncSendDataToPebbleApp("4dab81a6-d2fc-458a-992c-7a1f3b96a970", {'0': '02:33', '1': '10.22', '2': '3.3'});
            }
            if (iCheckPebbleStep === 5)
            {
                progressBarCheckPebble.label = qsTr("sending data 3");
                pebbleComm.fncSendDataToPebbleApp("4dab81a6-d2fc-458a-992c-7a1f3b96a970", {'0': '02:45', '1': '11.34', '2': '14.4'});
            }
            if (iCheckPebbleStep === 6)
            {
                progressBarCheckPebble.label = qsTr("closing sport app");
                pebbleComm.fncClosePebbleApp("4dab81a6-d2fc-458a-992c-7a1f3b96a970");
            }
            if (iCheckPebbleStep === 7)
            {
                progressBarCheckPebble.label = "";
                iCheckPebbleStep = 0;
            }
        }
    }

    Rectangle
    {
        visible: false
        id: id_REC_Rockpool
        z: 2
        color: "black"
        anchors.fill: parent
        Label
        {
            id: id_LBL_Rockpool
            color: "red"
            text: qsTr("Rockpool must be installed<br>at least in version 1.4-1.<br>Installed version is: ")
            font.pixelSize: Theme.fontSizeSmall
            anchors.centerIn: parent
        }
        Label
        {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: id_LBL_Rockpool.bottom
            anchors.topMargin: Theme.paddingLarge
            font.pixelSize: Theme.fontSizeSmall
            property string urlstring: "https://openrepos.net/content/abranson/rockpool"
            text: "<a href=\"" + urlstring + "\">" +  urlstring + "<\a>"
            onLinkActivated: Qt.openUrlExternally(link)
        }
        Image
        {
            width: parent.width/10
            height: parent.width/10
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: id_LBL_Rockpool.top
            anchors.bottomMargin: Theme.paddingLarge
            source: "../icons/cancel-red.png"
        }
    }

    Rectangle
    {
        visible: false
        id: id_REC_PebbleAddress
        z: 2
        color: "black"
        anchors.fill: parent
        Label
        {
            id: id_LBL_PebbleAddress
            color: "red"
            text: qsTr("No Pebble found.<br>Install Rockpool and<br>then connect Pebble!")
            font.pixelSize: Theme.fontSizeSmall
            anchors.centerIn: parent
        }
        Label
        {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: id_LBL_PebbleAddress.bottom
            anchors.topMargin: Theme.paddingLarge
            font.pixelSize: Theme.fontSizeSmall
            property string urlstring: "https://openrepos.net/content/abranson/rockpool"
            text: "<a href=\"" + urlstring + "\">" +  urlstring + "<\a>"
            onLinkActivated: Qt.openUrlExternally(link)
        }
        Image
        {
            width: parent.width/10
            height: parent.width/10
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: id_LBL_PebbleAddress.top
            anchors.bottomMargin: Theme.paddingLarge
            source: "../icons/cancel-red.png"
        }
    }

    Rectangle
    {
        visible: (iCheckPebbleStep > 0)
        anchors.fill: parent
        color: "black"
        z: 2

        ProgressBar
        {
            id: progressBarCheckPebble
            width: parent.width
            anchors.centerIn: parent
            maximumValue: 6
            valueText: value.toString() + "/" + maximumValue.toString()
            label: ""
            visible: (iCheckPebbleStep > 0)
            value: iCheckPebbleStep
        }
    }


    SilicaFlickable
    {
        anchors.fill: parent
        contentHeight: column.height + Theme.paddingLarge;
        VerticalScrollDecorator {}

        Column
        {
            id: column
            width: page.width
            spacing: Theme.paddingLarge
            PageHeader
            {
                title: bPebbleConnected ? sPebbleNameAddress : qsTr("Pebble settings")
            }
            TextSwitch
            {
                id: id_TextSwitch_enablePebble
                text: qsTr("Enable Pebble support")
                description: qsTr("View workout data on Pebble Smartwatch.")
            }

            ComboBox
            {
                id: id_CMB_ChoosePebble
                visible: false
                width: parent.width
                menu: ContextMenu { Repeater { model: arComboboxStringArray; MenuItem { text: modelData }}}
                description: qsTr("Choose the Pebble you want to use")
            }

            Separator
            {
                visible: id_TextSwitch_enablePebble.checked
                color: Theme.highlightColor
                width: parent.width
                horizontalAlignment: Qt.AlignHCenter
            }
            Item
            {

                width: parent.width
                height: id_BTN_TestPebble.height

                Label
                {
                    id: id_LBL_PebbleConnected
                    anchors.left: parent.left
                    anchors.leftMargin: Theme.paddingSmall
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Connected:")
                }

                GlassItem
                {
                    id: id_GI_PebbleConnected
                    anchors.left: id_LBL_PebbleConnected.right
                    anchors.verticalCenter: parent.verticalCenter
                    color: bPebbleConnected ? "green" : "red"
                    falloffRadius: 0.15
                    radius: 1.0
                    cache: false
                }
                Button
                {
                    id: id_BTN_TestPebble
                    text: qsTr("Test Pebble")
                    width: parent.width/2
                    anchors.right: parent.right
                    anchors.rightMargin: Theme.paddingSmall
                    onClicked:
                    {
                        iCheckPebbleStep = 1;
                        progressBarCheckPebble.label = qsTr("starting sport app");
                        pebbleComm.fncLaunchPebbleApp("4dab81a6-d2fc-458a-992c-7a1f3b96a970");
                    }
                }
            }

            Separator
            {
                visible: id_TextSwitch_enablePebble.checked
                color: Theme.highlightColor
                width: parent.width
                horizontalAlignment: Qt.AlignHCenter
            }
            Label
            {
                wrapMode: Text.WordWrap
                color: Theme.primaryColor
                visible: id_TextSwitch_enablePebble.checked
                text: qsTr("Choose values for Pebble fields!")
            }
            ComboBox
            {
                visible: id_TextSwitch_enablePebble.checked
                id: id_CMB_ValueField1
                label: qsTr("1 DURATION field:")
                menu: ContextMenu { Repeater { model: JSTools.arrayPebbleValueTypes; MenuItem { text: modelData.header } }}
            }
            ComboBox
            {
                visible: id_TextSwitch_enablePebble.checked
                id: id_CMB_ValueField2
                label: qsTr("2 DISTANCE field:")
                menu: ContextMenu { Repeater { model: JSTools.arrayPebbleValueTypes; MenuItem { text: modelData.header } }}
            }
            ComboBox
            {
                visible: id_TextSwitch_enablePebble.checked
                id: id_CMB_ValueField3
                label: qsTr("3 PACE/SPEED field:")
                menu: ContextMenu { Repeater { model: JSTools.arrayPebbleValueTypes; MenuItem { text: modelData.header } }}
            }
        }
    }
}
