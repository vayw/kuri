/*
 * Copyright (C) 2018 Jens Drescher, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.5
import QtQuick.Layouts 1.1
import Sailfish.Silica 1.0
import harbour.kuri 1.0
import "../tools/JSTools.js" as JSTools
import "../graph/"
import "../components/"

Page
{
    id: root

    allowedOrientations: Orientation.Portrait

    property bool bLockFirstPageLoad: true
    property bool bPaceRelevantForWorkoutType: true
    property bool bHeartrateSupported: false
    property variant arHeartrateData
    property variant arElevationData
    property variant arSpeedData
    property variant arPaceData
    property int iMinValueElevation: 0
    property int iMaxValueElevation: 0
    property int iMaxValueSpeed: 0
    property int iMaxValueHeartrate: 0

    property var trackLoader: undefined
    property var trackSlider: undefined

    property bool measureSystemIsMetric: true

    Component.onCompleted:
    {
        var iLastProperHeartRate = 0;
        var arrayHeartrateData = [];
        var arrayElevationData = [];
        var arraySpeedData = [];
        var arrayPaceData = [];

        for (var i = 0; i < trackLoader.trackPointCount(); i++)
        {
            var iHeartrate = 0;
            var iElevation = measureSystemIsMetric ? trackLoader.elevationAt(i) : JSTools.fncConvertelevationToImperial(trackLoader.elevationAt(i));
            var iSpeed = measureSystemIsMetric? trackLoader.speedAt(i) : (JSTools.fncConvertSpeedToImperial(trackLoader.speedAt(i)));
            var iPace = measureSystemIsMetric ? trackLoader.paceAt(i) : (JSTools.fncConvertPacetoImperial(trackLoader.paceAt(i)));

            var tmpHeartrate = trackLoader.heartRateAt(i);
            if (tmpHeartrate > 0)
            {
                iHeartrate = tmpHeartrate;
                iLastProperHeartRate = tmpHeartrate;
            }
            else
            {
                iHeartrate = iLastProperHeartRate;
            }

            //Calculate min/max values for elevation
            if (iElevation > iMaxValueElevation)
                iMaxValueElevation = iElevation;
            if (bLockFirstPageLoad || iElevation < iMinValueElevation)
                iMinValueElevation = iElevation;
            //Calculate max value for speed
            if (iSpeed > iMaxValueSpeed)
                iMaxValueSpeed = iSpeed;
            //Calculate max value for speed
            if (iHeartrate > iMaxValueHeartrate)
                iMaxValueHeartrate = iHeartrate;

            var unixTime = trackLoader.unixTimeAt(i);
            arrayHeartrateData.push({"x":unixTime,"y":iHeartrate});
            arrayElevationData.push({"x":unixTime,"y":iElevation});
            arraySpeedData.push({"x":unixTime,"y":iSpeed});
            arrayPaceData.push({"x":unixTime,"y":iPace});
        }

        //If min value for elevation is over 100 the diagram would not be painted :-(
        if (iMinValueElevation > 100)
            iMinValueElevation = 100;

        //max value for elevation need to be rounded to the next 50'er step
        console.log("Ele Max/Min: " + iMaxValueElevation.toString() + "/" + iMinValueElevation.toString());
        iMaxValueElevation = Math.ceil(iMaxValueElevation/50)*50;
        console.log("Ele Max/Min: " + iMaxValueElevation.toString() + "/" + iMinValueElevation.toString());

        console.log("Speed Max: " + iMaxValueSpeed.toString());
        iMaxValueSpeed = Math.ceil(iMaxValueSpeed/50)*50;
        console.log("Speed Max/Min: " + iMaxValueSpeed.toString());

        arHeartrateData = arrayHeartrateData;
        arElevationData = arrayElevationData;
        arSpeedData = arraySpeedData;
        arPaceData = arrayPaceData;

        fncUpdateGraphs();
    }

    onStatusChanged:
    {

        if (status === PageStatus.Active)
        {
            measureSystemIsMetric = settings.measureSystem === 0
            slider.maximumValue = trackLoader.trackPointCount() < 2 ? 1 : trackLoader.trackPointCount() - 1;
            slider.value = trackSlider.value
            fncUpdateGraphs();
        }

        if (status === PageStatus.Inactive)
        {
            trackSlider.value = slider.value
        }
    }

    function fncUpdateGraphs()
    {
        if (bHeartrateSupported) {
            graphHeartrate.updateGraph();
        }
        graphElevation.updateGraph();
        if (bPaceRelevantForWorkoutType) {
            graphPace.updateGraph();
        } else {
            graphSpeed.updateGraph();
        }
    }

    // had to re-add this since the graph was empty after minimizing to cover view -> find a proper fix
    ApplicationWindow
    {
        onApplicationActiveChanged:
        {
            console.log("applicationActive: " + applicationActive);
            if (applicationActive)
            {
                fncUpdateGraphs();
            }
        }
    }

    ColumnLayout
    {
        id: content
        spacing: Theme.paddingSmall
        anchors.fill: parent

        property int graphDataHeight: (root.height - header.height - separator.height - slider.height - pageIndicator.height - 8 * spacing) / (bHeartrateSupported ? 3 : 2)

        PageHeader
        {
            id: header
            Layout.fillWidth: true
            title: qsTr("Diagrams")
        }

        Item
        {
            Layout.fillHeight: true
        }

        GraphData
        {
            id: graphHeartrate
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true
            height: content.graphDataHeight

            enabled: bHeartrateSupported
            visible: enabled
            graphTitle: qsTr("Heartrate")

            axisY.units: "bpm"

            function updateGraph()
            {
                setPoints(arHeartrateData);
            }

            bShowCurrentLine: true
            iCurrentLinePosition: enabled ? ((100.0 / trackLoader.trackPointCount()) * slider.value) : 0
            iCurrentIndex: enabled ? slider.value.toFixed(0) : 0

            lineWidth: 1
            minY: 0
            maxY: 200
            valueConverter: function(value)
            {
                return value.toFixed(0);
            }
            onClicked:
            {
                updateGraph();
            }
        }
        GraphData
        {
            id: graphElevation
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true
            height: content.graphDataHeight

            graphTitle: qsTr("Elevation")

            axisY.units: measureSystemIsMetric ? "m" : "ft"

            function updateGraph()
            {
                setPoints(arElevationData);
            }

            bShowCurrentLine: true
            iCurrentLinePosition: ((100.0 / trackLoader.trackPointCount()) * slider.value);
            iCurrentIndex: slider.value.toFixed(0)

            lineWidth: 2
            minY: 0
            maxY: iMaxValueElevation
            valueConverter: function(value)
            {
                return value.toFixed(0);
            }
            onClicked:
            {
                updateGraph();
            }
        }
        GraphData
        {
            id: graphSpeed
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true
            height: content.graphDataHeight

            enabled: !bPaceRelevantForWorkoutType
            visible: enabled
            graphTitle: qsTr("Speed")

            axisY.units: measureSystemIsMetric ? "km/h" : "mi/h"

            function updateGraph()
            {
                setPoints(arSpeedData);
            }

            bShowCurrentLine: true
            iCurrentLinePosition: enabled ? ((100.0 / trackLoader.trackPointCount()) * slider.value) : 0
            iCurrentIndex: enabled ? slider.value.toFixed(0) : 0

            lineWidth: 2
            minY: 0
            maxY: iMaxValueSpeed
            valueConverter: function(value)
            {
                return value.toFixed(1);
            }
            onClicked:
            {
                updateGraph();
            }
        }
        GraphData
        {
            id: graphPace
            Layout.alignment: Qt.AlignCenter
            Layout.fillWidth: true
            height: content.graphDataHeight

            enabled: bPaceRelevantForWorkoutType
            visible: enabled
            graphTitle: qsTr("Pace")

            axisY.units: measureSystemIsMetric ? "min/km" : "min/mi"

            function updateGraph()
            {
                setPoints(arPaceData);
            }

            bShowCurrentLine: true
            iCurrentLinePosition: enabled ? ((100.0 / trackLoader.trackPointCount()) * slider.value) : 0
            iCurrentIndex: enabled ? slider.value.toFixed(0) : 0

            lineWidth: 2
            minY: 0.0
            maxY: 12.0
            valueConverter: function(value)
            {
                return value.toFixed(1);
            }
            onClicked:
            {
                updateGraph();
            }
        }

        Item
        {
            Layout.fillHeight: true
        }

        Separator
        {
            id: separator
            Layout.fillWidth: true

            color: Theme.secondaryHighlightColor
            horizontalAlignment: Qt.AlignHCenter
        }

        TrackSlider {
            id: slider
            Layout.fillWidth: true
            measureSystemIsMetric: root.measureSystemIsMetric

            minimumValue: 0
            maximumValue: 1
        }

        RowLayout {
            id: pageIndicator
            Layout.fillWidth: true
            spacing: radiusSmall

            readonly property int radiusSmall: Theme.paddingSmall
            readonly property int radius: 1.6 * radiusSmall

            Item {
                Layout.fillWidth: true
                height: 7 * pageIndicator.radiusSmall
            }

            Rectangle {
                radius: pageIndicator.radiusSmall
                width: 2 * radius
                height: width
                Layout.alignment: Qt.AlignVCenter

                color: Theme.secondaryColor
            }

            Rectangle {
                radius: pageIndicator.radius
                width: 2 * radius
                height: width
                Layout.alignment: Qt.AlignVCenter

                color: Theme.primaryColor
            }

            Item {
                Layout.fillWidth: true
            }
        }
    }
}
