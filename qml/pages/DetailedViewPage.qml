/*
 * Copyright (C) 2017 Jens Drescher, Germany
 * Copyright (C) 2021 Mathias Kraus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.5
import QtQuick.Layouts 1.1
import Sailfish.Silica 1.0
import harbour.kuri 1.0
import QtPositioning 5.2
import "../tools/JSTools.js" as JSTools
import "../tools/SportsTracker.js" as ST
import "../tools/SharedResources.js" as SharedResources
import com.pipacs.o2 1.0
import "../components/"

Page
{
    id: root
    allowedOrientations: Orientation.Portrait

    //No back navigation if the map is big
    backNavigation: !map.maximized
    forwardNavigation: !map.maximized

    property var track: undefined

    property int stSharing: 0
    property string stComment: ""
    property var vTrackLinePoints

    property int iCurrentWorkout: 0

    property bool bHeartrateSupported: false
    property bool bPaceRelevantForWorkoutType: true
    property int iPausePositionsCount: 0

    property bool measureSystemIsMetric: true

    onStatusChanged:
    {
        if (status === PageStatus.Active)
        {
            trackLoader.filename = track.filename;
            map.mapStyle = settings.mapStyle;
            measureSystemIsMetric = settings.measureSystem === 0
        }
    }

    function displayNotification(text, type, delay){
        console.log(text);
        load_text.text = text;
        if (type === "info"){
            ntimer.interval = delay;
            load_text.color = Theme.primaryColor;
        }
        else if (type === "success"){
            ntimer.interval = delay;
            load_text.color = Theme.primaryColor;
        }
        else if (type === "error"){
            if (ST.loginstate == 1 && ST.recycledlogin === true){
                console.log("Sessionkey might be too old. Trying to login again");
                settings.stSessionkey = "";
                ST.SESSIONKEY = "";
                recycledlogin = false;
                ST.uploadToSportsTracker(stSharing, stComment, displayNotification);
            }
            else{
                ntimer.interval = delay;
                load_text.color = Theme.secondaryHighlightColor;
            }
        }
        ntimer.restart();
        ntimer.start();
    }

    Timer{
        id:ntimer;
        running: false;
        interval: 2000;
        repeat: false;
        onTriggered: {
            detail_busy.running = false;
            detail_flick.visible = true;
            detail_busy.visible = false;
            load_text.visible = false;
            ntimer.restart();
            ntimer.start();
        }
    }

    TrackLoader
    {
        id: trackLoader
        onTrackChanged:
        {
            var trackLength = trackLoader.trackPointCount();
            var pauseLength = trackLoader.pausePositionsCount();
            var iLastProperHeartRate = 0;

            JSTools.trackPausePointsTemporary = [];

            for(var i=0; i<trackLength; i++)
            {
                var iHeartrate = trackLoader.heartRateAt(i);

                //Problem is there are often HR points with value 0. This will be solved.
                if (iHeartrate > 0)
                {
                    iLastProperHeartRate = iHeartrate;
                }
                else
                {
                    iHeartrate = iLastProperHeartRate;
                }
            }

            //Go through array with pause data points
            for (i=0; i<pauseLength; i++)
            {
                //add this track point to temporary array in JS.
                JSTools.trackPausePointsTemporary.push(trackLoader.pausePositionAt(i));
            }

            bHeartrateSupported = trackLoader.hasHeartRateData();
            bPaceRelevantForWorkoutType = trackLoader.paceRelevantForWorkoutType();
            iPausePositionsCount = trackLoader.pausePositionsCount();

            slider.maximumValue = trackLoader.trackPointCount() < 2 ? 1 : trackLoader.trackPointCount() - 1;
            map.updateTrack();

            pageStack.pushAttached(Qt.resolvedUrl("DiagramViewPage.qml"),{ trackLoader: trackLoader, trackSlider: slider, bHeartrateSupported: bHeartrateSupported, bPaceRelevantForWorkoutType: bPaceRelevantForWorkoutType});
        }
    }

    BusyIndicator
    {
        id: detail_busy
        visible: false
        anchors.centerIn: root
        running: true
        size: BusyIndicatorSize.Large
    }
    Label {
         id:load_text
         width: parent.width
         anchors.top: detail_busy.bottom
         anchors.topMargin: 25;
         horizontalAlignment: Label.AlignHCenter
         visible: false
         text: qsTr("loading...")
         font.pixelSize: Theme.fontSizeMedium
    }

    BusyIndicator
    {
        visible: true
        anchors.centerIn: root
        running: !trackLoader.loaded
        size: BusyIndicatorSize.Large
    }

    SilicaFlickable
    {
        id:detail_flick
        anchors.fill: parent
        contentHeight: root.height
        contentWidth: root.width

        visible: trackLoader.loaded
        VerticalScrollDecorator {}


        PullDownMenu
        {
            id: menu
            visible: !map.maximized
            MenuItem
            {
                text: qsTr("Diagrams")
                onClicked: pageStack.push(Qt.resolvedUrl("DiagramViewPage.qml"))
                visible: false
            }
            MenuItem
            {
                text: qsTr("Edit workout")
                onClicked:
                {
                    iCurrentWorkout = SharedResources.fncGetIndexByName(trackLoader.workout);

                    var dialog = pageStack.push(id_Dialog_EditWorkout);
                    dialog.sName = trackLoader.name;
                    dialog.sDesc = trackLoader.description;
                    dialog.iWorkout = SharedResources.fncGetIndexByName(trackLoader.workout);

                    dialog.accepted.connect(function()
                    {
                        //Edit and save GPX file
                        var filename = track.filename;
                        trackLoader.vReadFile(filename);
                        trackLoader.vSetNewProperties(track.name, trackLoader.description, trackLoader.workout, dialog.sName, dialog.sDesc, dialog.sWorkout)
                        trackLoader.vWriteFile(filename);

                        //Set edited values to model
                        track.name = dialog.sName;
                        track.description = dialog.sDesc;
                        track.workout = dialog.sWorkout;
                    })
                }
            }
            MenuItem
            {
                text: qsTr("Send to Sports-Tracker.com")
                visible: settings.stUsername === "" ? false:true
                onClicked: {

                    var dialog = pageStack.push(Qt.resolvedUrl("SportsTrackerUploadPage.qml"),{ stcomment: track.description});
                    dialog.accepted.connect(function() {
                        detail_busy.running = true;
                        detail_busy.visible = true;
                        load_text.visible = true;
                        detail_flick.visible = false;

                        ST.uploadToSportsTracker(dialog.sharing*1, dialog.stcomment, displayNotification);
                    });
                    dialog.rejected.connect(function() {

                    });

                 }
            }
            MenuItem
            {
                text: qsTr("Send to Strava")
                visible: o2strava.linked
                onClicked: {

                    var dialog = pageStack.push(Qt.resolvedUrl("StravaUploadPage.qml"));
                    dialog.activityID = track.filename;
                    var gpx = trackLoader.readGpx();
                    dialog.gpx = gpx;
                    dialog.activityName = track.name;
                    dialog.activityDescription = track.description;
                    dialog.activityType = track.workout
                }

                O2 {
                    id: o2strava
                    clientId: STRAVA_CLIENT_ID
                    clientSecret: STRAVA_CLIENT_SECRET
                    scope: "activity:write,activity:read_all"
                    requestUrl: "https://www.strava.com/oauth/authorize"
                    tokenUrl: "https://www.strava.com/oauth/token"
                    refreshTokenUrl: "https://www.strava.com/api/v3/oauth/token"
                }
            }
        }

        ColumnLayout
        {
            spacing: 0
            anchors.fill: parent

            PageHeader
            {
                Layout.fillWidth: true
                visible: !map.maximized

                title: qsTr("Overview")
            }

            Text {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignCenter
                horizontalAlignment: truncated ? Text.AlignLeft : Text.AlignHCenter
                visible: !map.maximized

                font.pixelSize: Theme.fontSizeSmall
                color: Theme.highlightColor
                elide: Text.ElideRight

                text: trackLoader.timeLongStr
            }
            Text {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignCenter
                horizontalAlignment: Text.AlignHCenter
                visible: !map.maximized

                font.pixelSize: Theme.fontSizeMedium
                color: Theme.primaryColor
                wrapMode: Text.WordWrap

                text: track.name==="" ? SharedResources.arrayLookupWorkoutTableByName[workout].labeltext : track.name
            }
            Text {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignCenter
                horizontalAlignment: Text.AlignHCenter
                visible: !map.maximized && !(text === "")

                font.pixelSize: Theme.fontSizeSmall
                color: Theme.primaryColor
                wrapMode: Text.WordWrap

                text: track.description
            }

            RowLayout {
                visible: !map.maximized
                Layout.alignment: Qt.AlignCenter
                Layout.fillWidth: true
                InfoItem
                {
                    id: durationField
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillWidth: true
                    label: qsTr("Duration")
                    value: trackLoader.durationStr;
                    unit: qsTr("h:m:s")
                    condensed: false
                }
                InfoItem
                {
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillWidth: true
                    label: qsTr("Distance")
                    value: measureSystemIsMetric ? (trackLoader.distance/1000).toFixed(2) : JSTools.fncConvertDistanceToImperial(trackLoader.distance/1000).toFixed(2)
                    unit: measureSystemIsMetric ? "km" : "mi"
                    condensed: false
                }
            }

            Separator
            {
                visible: !map.maximized
                Layout.fillWidth: true

                color: Theme.secondaryHighlightColor
                horizontalAlignment: Qt.AlignHCenter
            }

            GridLayout {
                columns: 3
                rowSpacing: 0
                Layout.alignment: Qt.AlignCenter
                Layout.fillWidth: true
                visible: !map.maximized

                InfoItem {
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillWidth: true
                    label: qsTr("Speed ⌀")
                    value: measureSystemIsMetric ? (trackLoader.speed*3.6).toFixed(1) : (JSTools.fncConvertSpeedToImperial(trackLoader.speed*3.6)).toFixed(1)
                    unit: measureSystemIsMetric ? "km/h" : "mi/h"
                }
                InfoItem {
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillWidth: true
                    visible: bPaceRelevantForWorkoutType
                    label: qsTr("Pace ⌀")
                    value: measureSystemIsMetric ? trackLoader.paceStr : trackLoader.paceImperialStr
                    unit: measureSystemIsMetric ? "min/km" : "min/mi"
                }
                InfoItem {
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillWidth: true
                    label: qsTr("Heart Rate ⌀")
                    value: bHeartrateSupported ? trackLoader.heartRate.toFixed(1) : "---"
                    unit: "bpm"
                }
                Separator {
                    Layout.fillWidth: true
                    Layout.columnSpan: 3

                    color: Theme.secondaryHighlightColor
                    horizontalAlignment: Qt.AlignHCenter
                }
                InfoItem {
                    Layout.alignment: Qt.AlignCenter
                    Layout.fillWidth: true
                    label: qsTr("Pause")
                    value: iPausePositionsCount > 0 ? trackLoader.pauseDurationStr : "---"
                    unit: qsTr("h:m:s")
                }
            }

            MapView {
                id: map
                Layout.alignment: Qt.AlignCenter
                Layout.fillWidth: true
                Layout.fillHeight: true
                measureSystemIsMetric: root.measureSystemIsMetric

                currentPosition: trackLoader.trackPointAt(slider.value.toFixed(0))
            }

            TrackSlider {
                id: slider
                Layout.fillWidth: true
                measureSystemIsMetric: root.measureSystemIsMetric

                minimumValue: 0
                maximumValue: 1
            }

            // this functions a as spacer to align the TrackSlider from above with the one from DiagramViewPage, which has a spacing set to the ColumnLayout
            Item {
                Layout.fillWidth: true
                height: Theme.paddingSmall
            }

            RowLayout {
                id: pageIndicator
                Layout.fillWidth: true
                spacing: radiusSmall

                readonly property int radiusSmall: Theme.paddingSmall
                readonly property int radius: 1.6 * radiusSmall

                Item {
                    Layout.fillWidth: true
                    height: 7 * pageIndicator.radiusSmall
                }

                Rectangle {
                    radius: pageIndicator.radius
                    width: 2 * radius
                    height: width
                    Layout.alignment: Qt.AlignVCenter

                    color: Theme.primaryColor
                }

                Rectangle {
                    radius: pageIndicator.radiusSmall
                    width: 2 * radius
                    height: width
                    Layout.alignment: Qt.AlignVCenter

                    color: Theme.secondaryColor
                }

                Item {
                    Layout.fillWidth: true
                }
            }
        }
    }

    Component
    {
        id: id_Dialog_EditWorkout


        Dialog
        {
            property string sName
            property string sDesc
            property int iWorkout
            property string sWorkout

            canAccept: true
            acceptDestination: root
            acceptDestinationAction:
            {
                sName = id_TXF_WorkoutName.text;
                sDesc = id_TXF_WorkoutDesc.text;
                iWorkout = cmbWorkout.currentIndex;

                PageStackAction.Pop;
            }

            Flickable
            {
                width: parent.width
                height: parent.height
                interactive: false

                Column
                {
                    width: parent.width

                    DialogHeader { title: qsTr("Edit workout") }

                    TextField
                    {
                        id: id_TXF_WorkoutName
                        width: parent.width
                        label: qsTr("Workout name")
                        placeholderText: qsTr("Workout name")
                        text: sName
                        inputMethodHints: Qt.ImhNoPredictiveText
                        focus: true
                        horizontalAlignment: TextInput.AlignLeft
                    }
                    Item
                    {
                        width: parent.width
                        height: Theme.paddingLarge
                    }
                    TextField
                    {
                        id: id_TXF_WorkoutDesc
                        width: parent.width
                        label: qsTr("Workout description")
                        placeholderText: qsTr("Workout description")
                        text: sDesc
                        inputMethodHints: Qt.ImhNoPredictiveText
                        focus: true
                        horizontalAlignment: TextInput.AlignLeft
                    }
                    Item
                    {
                        width: parent.width
                        height: Theme.paddingLarge
                    }
                    Row
                    {
                        spacing: Theme.paddingSmall
                        width:parent.width;
                        Image
                        {
                            id: imgWorkoutImage
                            height: parent.width / 8
                            width: parent.width / 8
                            fillMode: Image.PreserveAspectFit
                        }
                        ComboBox
                        {
                            id: cmbWorkout
                            width: (parent.width / 8) * 7
                            label: qsTr("Workout:")
                            currentIndex: iCurrentWorkout
                            menu: ContextMenu
                            {
                                Repeater
                                {
                                    model: SharedResources.arrayWorkoutTypes;
                                    MenuItem { text: modelData.labeltext }
                                }
                            }
                            onCurrentItemChanged:
                            {
                                console.log("Workout changed!");

                                imgWorkoutImage.source = SharedResources.arrayWorkoutTypes[currentIndex].icon;
                                iWorkout = currentIndex;
                                sWorkout = SharedResources.arrayWorkoutTypes[currentIndex].name;
                            }
                        }
                    }
                }
            }
        }
    }
}
