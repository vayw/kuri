/*
 * Copyright (C) 2017 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


import QtQuick 2.0
import QtQml 2.2

RecordPageField {
    id: root

    property int currentHeartRate: 0

    function update() {
        currentHeartRate = recorder.currentHeartRate;
    }

    label: qsTr("Heart Rate")
    value: currentHeartRate <= 0 || currentHeartRate >= 9999 ? "---" : currentHeartRate
    unit: qsTr("bpm")

    Component.onCompleted: {
        recorder.currentHeartRateChanged.connect(root.update);
    }

    Component.onDestruction: {
        recorder.currentHeartRateChanged.disconnect(root.update);
    }
}
