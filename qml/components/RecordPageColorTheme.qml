/*
 * Copyright (C) 2020 Mathias Kraus, Germany
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

pragma Singleton

import QtQuick 2.0
import QtQml 2.2

import Sailfish.Silica 1.0

Item {
    id: root

    property int id: 0
    readonly property var colors: internal.colorSelection

    onIdChanged: {
        if (root.id < 0) {
            root.id = 0;
            internal.colorSelection = internal.silica;
        } else if (root.id > 3) {
            root.id = 3;
            internal.colorSelection = internal.night;
        }

        internal.setTheme();
    }

    function nextTheme() {
        var newId = root.id + 1;
        if (newId > 3) {
            newId = 0;
        }
        root.id = newId;
    }

    Item {
        id: internal

        property var colorSelection: silica

        function setTheme() {
            switch (root.id) {
                case 0: internal.colorSelection = internal.silica; break;
                case 1: internal.colorSelection = internal.amoled; break;
                case 2: internal.colorSelection = internal.lcd; break;
                case 3: internal.colorSelection = internal.night; break;
                default: internal.colorSelection = internal.silica; break;
            }
        }

        property var silica: {
            "primary": Theme.primaryColor,
            "secondary": Theme.secondaryColor,
            "highlight": Theme.highlightColor,
            "secondaryHighlight": Theme.secondaryHighlightColor,
            "background": "transparent",
            "lockScreen": "black",

            "startButton": "#47aa16",
            "pauseButton": "#aa9116",
            "lapButton": "#1679aa",
            "stopButton": "#aa1616"
        }

        property var amoled: {
            "primary": "white",
            "secondary": "#d5d5d5",
            "highlight": "#57a4de",
            "secondaryHighlight": "#647d86",
            "background": "black",
            "lockScreen": "black",

            "startButton": "#47aa16",
            "pauseButton": "#aa9116",
            "lapButton": "#1679aa",
            "stopButton": "#aa1616"
        }

        property var lcd: {
            "primary": "black",
            "secondary": "#5b5b5b",
            "highlight": "#2b526f",
            "secondaryHighlight": "#91b4d2",
            "background": "white",
            "lockScreen": "white",

            "startButton": "#47aa16",
            "pauseButton": "#aa9116",
            "lapButton": "#1679aa",
            "stopButton": "#aa1616"
        }

        property var night: {
            "primary": "#f50103",
            "secondary": "#d50103",
            "highlight": "#ff1937",
            "secondaryHighlight": "#f50103",
            "background": "black",
            "lockScreen": "black",

            "startButton": "#47aa16",
            "pauseButton": "#aa9116",
            "lapButton": "#1679aa",
            "stopButton": "#aa1616"
        }
    }
}
